# Platzom

Platzom es un idioma inventado para el curso de [Fundamentos de Javascript de Platzi](https://platzi.com/js)


## Descripción del idioma

- Si la palabra termina en "ar", se le quitan esos dos caracteres.
- Si la palabra empieza con "z" se la añade al final "pe".
- Si la palabra traducida tiene 10 o más letras se debe dividir a la mitad y unir con un guión.
- Si la palabra original es un palindromo, ninguna regla anterior aplica y se devuelve la misma palabra intercalando mayúsculas y minúsculas.


## Instalación

```
npm install platzom
```


## Uso

```
import platzom from 'platzom'

platzom("Programar") 	//Program
platzom("Zorro")		//Zorrope
platzom("Zarpar")		//Zarppe
platzom("Abecedario")	//Abece-dario
platzom("Sometemos")	//SoMeTeMoS
```


## Créditos
- [Sacha Lifszyc](https://twitter.com/@slifszyc)


## Liciencia

[MIT](https://opensource.org/licenses/MIT)