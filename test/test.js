const expect = require('chai').expect
const platzom = require('..').default

describe('#platzom', function(){
	it('Si la palabra termina en "ar", se le quitan esos dos caracteres.', function(){
		const translation = platzom("Programar")
		expect(translation).to.equal("Program")
	})

	it('Si la palabra empieza con "z" se la añade al final "pe".', function(){
		const translation01 = platzom("Zorro")
		const translation02 = platzom("Zarpar")

		expect(translation01).to.equal("Zorrope")
		expect(translation02).to.equal("Zarppe")
	})

	it('Si la palabra traducida tiene 10 o más letras se debe dividir a la mitad y unir con un guión.', function(){
		const translation = platzom("Abecedario")
		expect(translation).to.equal("Abece-dario")
	})

	it('Si la palabra original es un palindromo, ninguna regla anterior aplica y se devuelve la misma palabra intercalando mayúsculas y minúsculas.', function(){
		const translation = platzom("Sometemos")
		expect(translation).to.equal("SoMeTeMoS")	
	})
})