export default function platzom(str){
	let translation = str

	//Sí la palabra termina en 'ar', se le quitan esos dos caracteres
	if(str.toLowerCase().endsWith('ar')){
		translation = str.slice(0, -2)
	}

	//Sí la palabra empieza con 'z' se la añade al final 'pe'
	if(str.toLowerCase().startsWith('z')){
		translation += 'pe'
	}

	//Sí la palabra traducida tiene 10 o más letras se debe
	//dividir a la mitad y unir con un guión.
	const length = translation.length
	if(length >= 10){
		const firstHalf = translation.slice(0, Math.round(length/2))
		const secondHalf = translation.slice(Math.round(length/2))
		translation = `${firstHalf}-${secondHalf}`
	}

	//Sí la palabra original es un palindromo, ninguna regla anterior
	//aplica y se devuelve la misma palabra intercalando mayúsculas y minúsculas.
	const reverse = (str) => str.split('').reverse().join('')

	function minMay(str){
		const length = str.length
		let translation = ''
		let capitalize = true
		//str = str.toLowerCase()
		for (let i = 0; i < length; i++) {
			const char = str.charAt(i)
			translation += capitalize ? char.toUpperCase() : char.toLowerCase()
			capitalize = !capitalize
		}

		return translation
	}

	if(str.toLowerCase() == reverse(str).toLowerCase()){
		return minMay(str)
	}

	return translation
}